This was part of my Master's Thesis, which was about slapd graceful restart.

The idea is to start second slapd process on another port and redirect all traffic to it before the first one restarts.

It consists of three scripts, with ldapfe_graceful.sh being the one doing the actual restart.