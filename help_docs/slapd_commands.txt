/usr/sbin/slapd -h ldap:/// ldapi:/// -g openldap -u openldap -F /etc/ldap/slapd.d --> commanda koja se upali na bootu

sudo /etc/init.d/slapd stop - stopiranje slapd-a koji se pokrene pri bootu (isto tako se taj isti starta sa sudo /etc/init.d/slapd start)

# slapd proces na FT nodeovima:
/opt/ericsson/cudb/ldapfe/libexec/slapd -f /home/cudb/dataAccess/ldapAccess/ldapFe/config/slapd.conf -h ldap://:389/ ldaps://:636/ ldapi://%2Ftmp%2Fslapd.sock

ldapsearch -x -LLL -b dc=example,dc=com -h 127.0.0.1 -p 400 'uid=jim' - mora se specifirat host i port!!

kill -2 (INT - ekvivalent ctrl + C) ili kill -1 (HUP) se po Vickovim notesima preporuca za ubijanje slapd-a (HUP je vise graceful)

cudbLdapFeRestart - skripta na nodeu koja resetira slapd-ove na svim bladeovima

/etc/init.d/slapd -  skripta koja se pali pri bootu i pokrece slapd (isto tako ga stopira, restartira..)

/etc/default/slapd - skripta koja se sourca prije startanja slapd-a i postavlja potrebne parametre za nju, sadrzi varijablu SLAPD_NO_START koja se odkomentira  ako ne zelimo da se slapd pokrene na bootu ili ako opcenito zelimo sami pokretat i gasit slapd

/var/run/slapd/ - sadrzi slapd.pid i slapd.args (prvi govori koji je trenutni pid slapd procesa, a drugi s kojim je argumentima proces pokrenut)
/var/run (nekad i /run) opcenito sadrze informacije o procesima koji se trenutno vrte

Razlog zasto ne mogu trenutno pokrenit dva slapd procesa (na razlicitim portovima) je moguce jer pokretanje jednog stvara lockfile koji onemogucava da se pokrene drugi dok god je taj lockfile prisutan.

pstree - prikazuje procese u obliku stabla

process - running instance of a program

unutar slapd.conf filea na nodeu se nalazi gentlehup on, koji omogucava da se prestanu slusat novi incoming requesti i da nastavi processat current requeste,
ako se slapd ubije s -HUP flagom

sudo start-stop-daemon -S -x /usr/sbin/slapd -- -h ldap://127.0.0.1:389 -g openldap -u openldap -F /etc/ldap/slapd.d - pokrece slapd daemon kao iz skripte, svi flagovi poslani iza -- se direktno salju slapd-u kao i kad ga startamo bez start-stop-daemon

slapd se na nodeu vrti samo na payload bladeovima - pise unutar cudbLdapFeMonitorSrv skripte

sudo slapcat >> ~/repos/diplomski/Diplomski/ldap_files/database - napravi backup od ldap direktorija u obliku .ldif filea

sudo slaptest -F /etc/ldap/slapd.d - provjerava ispravnost ldap konfiguracije

/etc/init.d/cudbLDAPFrontEnd - skripta koja poziva cudbLdapFeMonitorSrv, koja poziva cudbLdapFeMonitor, koja starta slapd

/local2/log/ldapfe_errors -  logovi za ldapfe na nodeu

/etc/init.d/cudbLdapFeMonitorSrv - LdapFe monitor skripta

/opt/ericsson/cudb/Monitors/LdapFeMonitor/bin/cudbLdapFeMonitor - starta slapd

s bladea 10.22.174.3 - ldapsearch -h 10.22.174.4 -x -D "cn=manager,ou=ft,o=cudb,c=es" -b "ou=identities,ou=ft,o=cudb,c=es" -s subtree -p 389 -w normal - trazi ldap upit s bladea 10.22.174.4, na taj nacin se moze simulirat dolazak upita od vani

iptables -t nat -I PREROUTING -p tcp --dport 389 -j REDIRECT --to-port 400 - na bladeu 10.22.174.3 ce reroutat pakete s 389 na 400, testirano da radi
iptables -t nat -D PREROUTING 1 - brise pravilo reroutanja
kill -HUP PID - gracefully ubija slapd

rootDN="ou=ft,o=cudb,c=es" -- root entry na ft nodeovima, kod searcha se moze izostavit jer po defaultu krece od roota

ldapsearch -h 10.22.174.4 -w normal -x -D "cn=manager,ou=ft,o=cudb,c=es" - ldapsearch na nodeu di se bindamo s dn-om koji dolazi iza -D flaga
ldapsearch -h 10.22.174.4 -w normal -x -D "cn=manager,ou=ft,o=cudb,c=es" -d 1 - search s debug flagom, broj oznacava detaljnost debuga (ja mislin)

/var/log/ldapfe - jos jedni logovi za ldapfe

Da bi drugi slapd proces napravija svoj pid, potrebno je kopirat /home/cudb/dataAccess/ldapAccess/ldapFe/config/slapd.conf i izminit unutar njega pid i args
lokaciju. To se moze preko komande: sed -i 's/slapd.pid/slapd2.pid/g; s/slapd.args/slapd2.args/g' slapd2.conf

opt/ericsson/cudb/Monitors/LdapFeMonitor/bin/cudbLdapFeMonitor --cpu-set 0x00000001 --arguments -f|/home/cudb/dataAccess/ldapAccess/ldapFe/config/slapd.conf|-h|ldap://:389/ ldaps://:636/ ldapi://%2Ftmp%2Fslapd.sock --ldap-uri ldap://PL_2_4 --config-file /opt/ericsson/cudb/Monitors/LdapFeMonitor/etc/cudbLdapFeMonitor.conf

/etc/init.d/cudbLDAPFrontEnd stop - stopiranje slapd procesa

maknit -x prava na LdapFeMonitorima na pl-u i sc-ovima (oni dizu slapd)
SC - /opt/ericsson/cudb/Monitors/ldapFeMonitor/bin/cudb_LdapFeMonitord
PL - /opt/ericsson/cudb/Monitors/LdapFeMonitor/bin/cudbLdapFeMonitor

- slapadd se koristi najcesce za pocetno punjenje baze, ne moze se koristit dok se slapd vrti te se mora vrtit di se nalazi slapd server
- ldapadd je za dodavanje fileove kad baza vec postoji te se moze koristit sa serverom koji je bilo di


ldapadd -x -c -h 10.22.127.4 -p 389 -D "cn=manager,ou=ft,o=cudb,c=es" -w normal -f /cluster/home/cudb/storageEngine/config/initData/pl/fixed-entries-pl.ldif - puni slapd bazu na portu 389 s pocetnim podacima iz ldif filea

ldapsearch -x -LLL -D "cn=manager,ou=ft,o=cudb,c=es" -w normal -h 10.22.127.4 -p 389 -b 'ou=ft,o=cudb,c=es' - searcha po bazi pocevsi od -b
