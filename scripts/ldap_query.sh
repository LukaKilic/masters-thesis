#!/bin/bash

for i in $(seq 20000); do
    ldapsearch -x -LLL -D "cn=manager,ou=ft,o=cudb,c=es" \
      -w normal -h 10.22.127.4 -p 389 \
      -b 'dc=IMSI, ou=identities,ou=ft,o=cudb,c=es' 2>&1
done | while IFS= read -r line
do 
  echo "[---- PL0 ----] $(date) $line";  
done >> search_imsi_$(date '+%Y_%m_%d__%H_%M_%S').log
