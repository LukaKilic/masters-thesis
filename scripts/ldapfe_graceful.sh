#!/bin/bash

function executed() {
    if [ ${1} -ne 0 ]
    then
        echo "Failed!"
        cleanup
        exit 
    else
        echo "Succeeded!"
    fi
}


function cleanup() {
    echo "Cleaning up ..."

    if [ -f /local/cudb/slapd/slapd2.pid ]
    then
        echo "Killing slapd2 ..."
        kill -HUP $(cat /local/cudb/slapd/slapd2.pid)
    fi
    
    if [ -f /home/cudb/dataAccess/ldapAccess/ldapFe/config/slapd2.conf ]
    then
        echo "Deleting slapd2.conf file ..."
        rm /home/cudb/dataAccess/ldapAccess/ldapFe/config/slapd2.conf
    fi

    if [ ! -f /local/cudb/slapd/slapd.pid ]
    then
        echo "Starting slapd ..."
        start-stop-daemon --start --exec \
        /opt/ericsson/cudb/ldapfe/libexec/slapd \
        -- -f /home/cudb/dataAccess/ldapAccess/ldapFe/config/slapd.conf -h \
        ldap://:389/ >> /dev/null
    fi
    
    if [ "$(iptables -t nat -L | grep REDIRECT)" ]
    then
        iptables -t nat -D PREROUTING 1
    fi        

    echo "Done"
}


function is_alive() {
    counter=15
    flag=0
    while [ ${counter} -gt 0 ]
    do
        if [ ! -f /local/cudb/slapd/${1}.pid ]        
        then
            echo "${1} still not up, sleeping for 2 seconds ..."
            sleep 2
        else
            echo "Pidfile found, ${1} started"
            flag=1
            break
        fi
        let counter=counter-1
    done
    if [ ${flag} -eq 0 ]
    then
        echo "Slapd2 wasn't started, exiting ..."
        cleanup
        exit
    fi
}


function is_killed() {
    counter=60
    flag=0
    while [ ${counter} -gt 0 ]
    do
        if [ -f /local/cudb/slapd/${1}.pid ]
        then
            echo "${1} is still up, sleeping for 3 seconds"
            sleep 3
        else
            echo "${1} is down"
            flag=1
            break
        fi
        let counter=counter-1  
    done
    if [ ${flag} -eq 0 ]
    then
        echo "${1} is up after 180 seconds, killing with -9 signal ..."
        kill -9 $(cat /local/cudb/slapd/${1}.pid)
    fi
}


# copy slapd.conf
echo "Copying /home/cudb/dataAccess/ldapAccess/ldapFe/config/slapd.conf to \ 
/home/cudb/dataAccess/ldapAccess/ldapFe/config/slapd2.conf ..."
cp /home/cudb/dataAccess/ldapAccess/ldapFe/config/slapd.conf \
/home/cudb/dataAccess/ldapAccess/ldapFe/config/slapd2.conf
executed $?


# modify slapd.pid and slapd.args to slapd2.pid and slapd2.args
echo "Changing .pid and .args file for second slapd ..."
sed -i 's/slapd.pid/slapd2.pid/g; s/slapd.args/slapd2.args/g' \
/home/cudb/dataAccess/ldapAccess/ldapFe/config/slapd2.conf
executed $?


# start slapd daemon with new configuration
# this starts the /opt/ericsson/cudb/ldapfe/libexec/slapd daemon
# with arguments folowing --
# --name slapd2 is a workaround because start-stop-daemon won't start
# a program if it finds one running with the same name
# so we give it a "false" name to search, slapd2
echo "Starting second slapd ..."
start-stop-daemon --start --name slapd2 --exec \
/opt/ericsson/cudb/ldapfe/libexec/slapd \
-- -f /home/cudb/dataAccess/ldapAccess/ldapFe/config/slapd2.conf \
-h ldap://:400/ >> /dev/null
executed $?


# check if new pidfile was successfully created
# if it's not, exit the script
echo "Checking if new slapd was started successfully ..."
is_alive slapd2


# reroute packets to new slapd
echo "Rerouting packets to new slapd ..."
iptables -t nat -I PREROUTING -p tcp --dport 389 -j REDIRECT --to-port 400
executed $?


# gracefully shutdown original slapd
echo "Gracefully killing original slapd ..."
kill -HUP $(cat /local/cudb/slapd/slapd.pid)
executed $?


# wait until original slapd is shut down
echo "Waiting for original slapd to shut down ..."
is_killed slapd


# sleep 10 seconds for more realistic restart process
echo "Simulating real life LDAP-FE restart which takes some time, so we are \
sleeping for 20 seconds"
sleep 20


# time to bring back original slapd up
echo "Bringing original slapd back up ..."
start-stop-daemon --start --exec /opt/ericsson/cudb/ldapfe/libexec/slapd \
-- -f /home/cudb/dataAccess/ldapAccess/ldapFe/config/slapd.conf \
-h ldap://:389/ >> /dev/null
executed $?


# check that original slapd is up and running
# give it some time, but exit after timeout
echo "Checking if slapd is up ..."
is_alive slapd


# delete rerouting rule
echo "Deleting rerouting rule ..."
iptables -t nat -D PREROUTING 1
executed $?


# gracefully kill the slapd2
echo "Gracefully killing slapd2 ..."
kill -HUP $(cat /local/cudb/slapd/slapd2.pid)
executed $?


# check that slapd2 is killed
echo "Waiting for slapd2 to shut down ..."
is_killed slapd2


# cleaning up
cleanup

# exit
echo "Graceful restart finished, exiting..."
exit

