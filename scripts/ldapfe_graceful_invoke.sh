#!/bin/bash

./ldapfe_graceful.sh | while IFS= read -r line; do echo "$(date) $line"; done \
>> ldapfe_graceful_$(date '+%Y_%m_%d__%H_%M_%S').log 2>&1 &
